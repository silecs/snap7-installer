#!/bin/sh
set -e

# First argument has to be the desired snap7 version
SNAP7_VERSION=$1

# Second argument is the install location
RELEASE_DIR=$2

# Third argument has to be the desired build target (e.g. x86_64)
BUILD_TARGET=$3

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")     # path where this script is located in
SNAP7_NAME=snap7-full-${SNAP7_VERSION}
SNAP7_FOLDER_PATH=${SCRIPTPATH}/${SNAP7_NAME}

echo ${SNAP7_FOLDER_PATH}
# If there is no snap7 folder yet, we download the required version and build it
if [ -d ${SNAP7_FOLDER_PATH} ]; then
  echo "snap7 version is already available. No need to download it"
else
  
  archiveFile=${SNAP7_NAME}.tar.gz
  archiveFilePath=${SCRIPTPATH}/${SNAP7_NAME}.tar.gz

  echo "Cleanup"
  rm -rf snap7-ful*

  if [ ! -f "$archiveFilePath" ]
  then
     echo "Downloading snap7"
     cd ${SCRIPTPATH}
     curl -L http://downloads.sourceforge.net/project/snap7/${SNAP7_VERSION}/${archiveFile} > ${archiveFile}
  fi

  if [ ! -d ${SNAP7_FOLDER_PATH} ]
  then
     echo "Extracting snap7"
     tar -vxzf ${archiveFilePath}
     rm ${archiveFilePath}
  fi
fi

BUILD_DIR=${SCRIPTPATH}/build-${BUILD_TARGET}

if [[ $TARGET == yocto-linux ]]; then
    if [ ! -f "$YOCTO_SDK_ENV" ]; then
      echo "Error: Build environment for target '$TARGET' not found at '$YOCTO_SDK_ENV'."
      echo "configure aborted"
      exit 1
    fi

    # Configure yocto environment.
    unset LD_LIBRARY_PATH; source $YOCTO_SDK_ENV
fi

echo "Building snap7"
cp CMakeLists.txt ${SNAP7_FOLDER_PATH}/src
mkdir -p ${BUILD_DIR}
cmake -S ${SNAP7_FOLDER_PATH}/src -B ${BUILD_DIR}
cmake --build ${BUILD_DIR} -j 8

echo "Installing snap7"
mkdir -p ${RELEASE_DIR}/${BUILD_TARGET}/lib
mkdir -p ${RELEASE_DIR}/include
cp -r ${BUILD_DIR}/libsnap7* ${RELEASE_DIR}/${BUILD_TARGET}/lib
cp ${SNAP7_FOLDER_PATH}/release/Wrappers/c-cpp/snap7.h ${RELEASE_DIR}/include


