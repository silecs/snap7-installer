#!/bin/sh
set -e

# Allow to overwrite the location of the Yocto SDK
: "${YOCTO_SDK_ENV:="/common/usr/embedded/yocto/fesa/current/sdk/environment-setup-core2-64-ffos-linux"}"

if [ "$#" -lt 1 ]; then
  RELEASE_DIR_BASE=/common/usr/cscofe/silecs
else
  RELEASE_DIR_BASE=$1
fi

# Custom build target can be passed like this: BUILD_TARGET=yocto-linux ./install.sh
if [[ -v BUILD_TARGET ]]; then
  echo "### Using build target: ${BUILD_TARGET} ###"
else
  BUILD_TARGET=x86_64-linux
  echo "### No build target defined. Using default build target: ${BUILD_TARGET} ###"
fi

if [[ $BUILD_TARGET == yocto-linux ]]; then
    if [ ! -f "$YOCTO_SDK_ENV" ]; then
      echo "Error: Build environment for target '$BUILD_TARGET' not found at '$YOCTO_SDK_ENV'."
      echo "configure aborted"
      exit 1
    fi

    # Configure yocto environment.
    unset LD_LIBRARY_PATH; source ${YOCTO_SDK_ENV}
fi

SNAP7_VERSION_FULL=1.4.0
SNAP7_VERSION_MAJOR=`echo ${SNAP7_VERSION_FULL} | cut -d. -f1`
SNAP7_VERSION_MINOR=`echo ${SNAP7_VERSION_FULL} | cut -d. -f2`

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")     # path where this script is located in

RELEASE_DIR=${RELEASE_DIR_BASE}/snap7/${SNAP7_VERSION_FULL}
if [ -d ${RELEASE_DIR}/${BUILD_TARGET} ]; then 
    echo "Error: ${RELEASE_DIR}/${BUILD_TARGET} already exists ...skipping"
    exit 1
fi

# Download, build and install snap7 if required
${SCRIPTPATH}/build.sh ${SNAP7_VERSION_FULL} ${RELEASE_DIR} ${BUILD_TARGET}

# Make sure everybody can read all installed files
chmod a+r -R ${RELEASE_DIR}
chmod a+x -R ${RELEASE_DIR}

# Update link to latest version
LATEST_LINK=${RELEASE_DIR_BASE}/snap7/latest
if [ -L ${LATEST_LINK} ]; then
    rm ${LATEST_LINK}
fi
ln -fvs ${RELEASE_DIR_BASE}/snap7/${SNAP7_VERSION_FULL} ${LATEST_LINK}

# Update links to major version
MAJOR_LINK=${RELEASE_DIR_BASE}/snap7/${SNAP7_VERSION_MAJOR}
if [ -L ${MAJOR_LINK} ]; then
    rm ${MAJOR_LINK}
fi
ln -fvs ${RELEASE_DIR_BASE}/snap7/${SNAP7_VERSION_FULL} ${MAJOR_LINK}

# Update links to major.minor version
MAJOR_MINOR_LINK=${RELEASE_DIR_BASE}/snap7/${SNAP7_VERSION_MAJOR}.${SNAP7_VERSION_MINOR}
if [ -L ${MAJOR_MINOR_LINK} ]; then
    rm ${MAJOR_MINOR_LINK}
fi
ln -fvs ${RELEASE_DIR_BASE}/snap7/${SNAP7_VERSION_FULL} ${MAJOR_MINOR_LINK}



